//
//  AppDelegate.h
//  Stock quote
//
//  Created by sarika Redddy on 5/31/16.
//  Copyright © 2016 sarika Redddy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

