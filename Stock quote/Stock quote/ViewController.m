//
//  ViewController.m
//  Stock quote
//
//  Created by sarika Redddy on 5/31/16.
//  Copyright © 2016 sarika Redddy. All rights reserved.
//

#import "ViewController.h"
#import "Reachability.h"
@interface ViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property(nonatomic,strong)NSArray *arrayStockInfo;
@property(nonatomic,strong)NSDictionary *dicStockInfo;


@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    
    // Allocate a reachability object
    Reachability* reach = [Reachability reachabilityWithHostname:@"www.google.com"];
    
    // Set the blocks
    reach.reachableBlock = ^(Reachability*reach)
    {
        // keep in mind this is called on a background thread
        // and if you are updating the UI it needs to happen
        // on the main thread, like this:
        
        dispatch_async(dispatch_get_main_queue(), ^{
            NSLog(@"Network is reachble!");
           [self getStockQuote];
           
            
        });
    };
    
    reach.unreachableBlock = ^(Reachability*reach)
    {
        
        [self showAlert];


    };
    
    // Start the notifier, which will cause the reachability object to retain itself!
    [reach startNotifier];
    
    

}


-(void)showAlert
{
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Alert"
                                  message:@"Please check your network coneection"
                                  preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"Okay"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                                {
                                    //Handel your yes please button action here
                                    [self dismissViewControllerAnimated:YES completion:nil];
                                    
                                }];
    [alert addAction:yesButton];

    
    [self presentViewController:alert animated:YES completion:nil];
}



-(void)getStockQuote
{
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:@"https://www.google.com/finance/info?q=GM"] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSString* responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        responseString =[responseString stringByReplacingOccurrencesOfString:@"//" withString:@""];
        
        NSData* responseData = [responseString dataUsingEncoding:NSUTF8StringEncoding];
        
        _dicStockInfo = [[NSJSONSerialization JSONObjectWithData:responseData options:0 error:nil] objectAtIndex:0];
        NSLog(@"%@", _dicStockInfo);
        
         _arrayStockInfo=[_dicStockInfo allKeys];
        dispatch_async(dispatch_get_main_queue(), ^{
            [_tableView reloadData];
        });
        
        
    }];
    
    [dataTask resume];
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [_arrayStockInfo count];    //count number of row from counting array hear cataGorry is An Array
}



- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"MyTableViewCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                       reuseIdentifier:MyIdentifier];
    }
    
    // Here we use the provided setImageWithURL: method to load the web image
    // Ensure you use a placeholder image otherwise cells will be initialized with no image
   
    cell.textLabel.text = [NSString stringWithFormat:@"%@ : %@",[_arrayStockInfo objectAtIndex:indexPath.row],[_dicStockInfo valueForKey:[_arrayStockInfo objectAtIndex:indexPath.row]]];
    return cell;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
