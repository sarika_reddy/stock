//
//  main.m
//  Stock quote
//
//  Created by sarika Redddy on 5/31/16.
//  Copyright © 2016 sarika Redddy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
